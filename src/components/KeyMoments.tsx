import React, { useState, useEffect } from "react";
import {
  FlatList,
  Image,
  Pressable,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from "react-native";

interface Moment {
  time: number;
  key_moment_title: string;
  key_moment: number;
}
interface Props {
  data: Array<Moment>;
  onSelect: Function;
}

const KeyMoments: React.FunctionComponent<Props> = (props) => {
  const [isOpen, setState] = useState(false);
  const handleMomentPress = (time: number) => {
    props.onSelect && props.onSelect(time);
  };

  useEffect(() => {
    if (!isOpen) {
      // deselect the selected "key-moment" when the user collapses the Key Moments section
      props.onSelect && props.onSelect(0);
    }
  }, [isOpen]);

  const renderItem = ({ item }: { item: Moment }) => {
    if (!item.key_moment) return null;
    return (
      <TouchableOpacity
        activeOpacity={0.3}
        onPress={() => handleMomentPress(item.time)}>
        <View style={styles.momentContainer}>
          <Text style={styles.momentTime}>{item.time}</Text>
          <Text style={styles.momentTitle}>{item.key_moment_title}</Text>
        </View>
      </TouchableOpacity>
    );
  };

  return (
    <Pressable
      onPress={() => setState((prev) => !prev)}
      style={[styles.container, isOpen ? styles.isOpen : undefined]}>
      <View style={styles.header}>
        <Text style={styles.title}>Key Moments</Text>
        <View style={styles.buttonWrapper}>
          <Image
            source={require("../../assets/chevron.png")}
            style={[styles.icon, isOpen ? styles.rotateIcon : undefined]}
          />
        </View>
      </View>
      <FlatList
        data={props.data}
        renderItem={renderItem}
        contentContainerStyle={{ paddingHorizontal: 20, paddingVertical: 10 }}
        keyExtractor={(item) => `${item.time}-${item.key_moment_title}`}
      />
    </Pressable>
  );
};

export default KeyMoments;

const styles = StyleSheet.create({
  container: {
    elevation: 4,
    borderTopLeftRadius: 10,
    borderTopRightRadius: 10,
    backgroundColor: "#f5f3f2",
    marginHorizontal: 10,
    maxHeight: "40%",
    height: 50,
  },
  isOpen: {
    height: "auto",
  },
  header: {
    flexDirection: "row",
    justifyContent: "center",
    paddingVertical: 15,
    paddingHorizontal: 20,
  },
  title: {
    fontSize: 20,
    fontWeight: "700",
    textAlign: "center",
  },
  buttonWrapper: {
    position: "absolute",
    top: 15,
    right: 20,
    width: 30,
    height: 30,
    alignItems: "center",
  },
  icon: {
    height: 25,
    width: 25,
    transform: [{ rotate: "180deg" }],
  },
  rotateIcon: {
    transform: [{ rotate: "0deg" }],
  },
  momentContainer: {
    flexDirection: "row",
    paddingVertical: 10,
  },
  momentTime: {
    fontSize: 18,
    fontWeight: "700",
  },
  momentTitle: {
    fontSize: 18,
    marginLeft: 30,
  },
});
