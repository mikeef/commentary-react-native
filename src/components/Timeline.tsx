import React, { useRef, useEffect } from "react";
import { FlatList, StyleSheet } from "react-native";
import Comment from "./Comment";

interface Props {
  data: Array<{ details: string; time: number }>;
  containerStyles?: object;
  selected?: number;
}

interface Item {
  time: number;
  details: string;
}

const Timeline: React.FunctionComponent<Props> = (props) => {
  const flatlistRef = useRef<FlatList>(null);

  useEffect(() => {
    // scroll to selected "key moment"
    if (flatlistRef.current && props.selected) {
      const selectedItem = props.data.find((data) => data.time === props.selected);
      flatlistRef.current.scrollToItem({ animated: true, item: selectedItem });
    }
  }, [props.selected]);

  const renderItem = ({ item, index }: { item: Item; index: number }) => {
    const selectedIndex = props.data.findIndex((data) => data.time === props.selected);
    const isSelected = selectedIndex === index;
    return (
      <Comment
        time={item.time}
        details={item.details}
        showProgressBar={index + 1 !== props.data.length}
        isSelected={isSelected}
      />
    );
  };

  return (
    <FlatList
      data={props.data}
      renderItem={renderItem}
      keyExtractor={(item) => `${item.time}`}
      contentContainerStyle={[styles.contentContainer, props.containerStyles]}
      style={styles.content}
      ref={flatlistRef}
    />
  );
};

Timeline.defaultProps = {
  containerStyles: undefined,
};

const styles = StyleSheet.create({
  contentContainer: {
    paddingBottom: 40,
    paddingHorizontal: 30,
    marginTop: 20,
  },
  content: {},
});

export default Timeline;
