import React from "react";
import { View, Text, StyleSheet } from "react-native";

interface Props {
  details: string;
  showProgressBar?: boolean;
  time: number;
  isSelected?: boolean;
}

const Comment: React.FunctionComponent<Props> = (props) => {
  return (
    <View
      style={[styles.container, !props.showProgressBar ? styles.noBorder : undefined]}>
      <View
        style={[
          styles.timeContainer,
          props.isSelected ? styles.hightlightTime : undefined,
        ]}>
        <Text style={styles.time}>{props.time}</Text>
      </View>
      <View
        style={[
          styles.detailsContainer,
          props.isSelected ? styles.hightlight : undefined,
        ]}>
        <Text style={styles.details}>{props.details}</Text>
      </View>
    </View>
  );
};

Comment.defaultProps = {
  showProgressBar: true,
};

const styles = StyleSheet.create({
  container: {
    flexDirection: "row",
    borderLeftWidth: 1,
    borderLeftColor: "#ccc9c8", // "#7a7979",
    borderTopRightRadius: 10,
    borderBottomRightRadius: 10,
    paddingBottom: 30,
    paddingTop: 10,
  },
  timeContainer: {
    left: -20,
    top: -10,
    width: 40,
    height: 40,
    alignItems: "center",
    justifyContent: "center",
    borderColor: "#ffffff",
    borderRadius: 20,
    backgroundColor: "#f5f3f2",
    elevation: 1,
  },
  time: {
    fontWeight: "700",
  },
  detailsContainer: {
    flex: 1,
    backgroundColor: "#f5f3f2",
    paddingVertical: 10,
    paddingHorizontal: 10,
    borderRadius: 10,
    marginTop: -10,
    elevation: 1,
  },
  details: {},
  noBorder: {
    borderLeftWidth: 0,
  },
  hightlight: {
    backgroundColor: "#ace3c2",
  },
  hightlightTime: {
    backgroundColor: "#ace3c2", //"#a0f2c1",
  },
});

export default Comment;
