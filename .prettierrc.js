module.exports = {
  printWidth: 87,
  bracketSpacing: true,
  jsxBracketSameLine: true,
  singleQuote: false,
  trailingComma: 'all',
};
