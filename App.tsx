import React, { useState, useEffect, useCallback } from "react";
import {
  SafeAreaView,
  StyleSheet,
  View,
  Text,
  StatusBar,
  ActivityIndicator,
} from "react-native";
import Timeline from "./src/components/Timeline";
import KeyMoments from "./src/components/KeyMoments";

interface Comment {
  time: number;
  details: string;
  key_moment: number;
  key_moment_title: string;
}

const App: () => React.ReactNode = () => {
  const [selectedMoment, setSelected] = useState<number>();
  const [comments, setCommetns] = useState<Array<Comment>>();
  const [loading, setLoading] = useState(false);

  // fetch data
  const fetchData = useCallback(() => {
    try {
      setLoading(true);
      setTimeout(() => {
        const response = require("./data.json");
        setCommetns(response.data);
        setLoading(false);
      }, 2000);
    } catch (e) {
      console.warn(e.message);
    }
  }, []);

  useEffect(() => {
    fetchData();
  }, []);

  return (
    <>
      <StatusBar barStyle="light-content" />
      <SafeAreaView style={styles.container}>
        <Text style={styles.title}>Live Commentary</Text>
        {loading ? (
          <View style={{ flex: 1, justifyContent: "center" }}>
            <ActivityIndicator size="large" color="#a0f2c1" />
          </View>
        ) : (
          <>
            <View style={{ flex: 1 }}>
              <Timeline data={comments ?? []} selected={selectedMoment} />
            </View>
            <KeyMoments
              data={comments ?? []}
              onSelect={(time: number) => setSelected(time)}
            />
          </>
        )}
      </SafeAreaView>
    </>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    marginTop: StatusBar.currentHeight || 0,
  },
  title: {
    fontSize: 20,
    fontWeight: "700",
    textAlign: "center",
    padding: 10,
  },
});

export default App;
