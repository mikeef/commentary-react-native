### How do I get set up?

- Install dependencies: npm install
- Android: npm run android
- iOS: npm run ios

### Note

The application has been tested only on Android device and emulator. Probably, there won't be any problem for iOS.

- The project has been set up using react-native CLI

  - src/components/ UI Components
  - data.json Hard-code JSON file for providing data.
  - index.js Main entry point of the app
